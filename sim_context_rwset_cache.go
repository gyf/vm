package vm

import "chainmaker.org/chainmaker/pb-go/v2/common"

type rwSet struct {
	txReadKeyMap  map[string]*common.TxRead  // txReadKeyMap mapped key with TxRead
	txWriteKeyMap map[string]*common.TxWrite // txWriteKeyMap mapped key with TxWrite
}

// collectReadKey collect readMap key into slice
func (rw *rwSet) collectReadKey() []string {
	readKeys := make([]string, 0, 8)

	for readKey := range rw.txReadKeyMap {
		readKeys = append(readKeys, readKey)
	}

	return readKeys
}

// collectWriteKey collect writeMap key into slice
func (rw *rwSet) collectWriteKey() []string {
	writeKeys := make([]string, 0, 8)

	for writeKey := range rw.txWriteKeyMap {
		writeKeys = append(writeKeys, writeKey)
	}

	return writeKeys
}

// mergeTxReadSet merge read map
func mergeTxReadSet(dst, src map[string]*common.TxRead) map[string]*common.TxRead {
	for key, txRead := range src {
		dst[key] = txRead
	}

	return dst
}

// mergeTxWriteSet merge write map
func mergeTxWriteSet(dst, src map[string]*common.TxWrite) map[string]*common.TxWrite {
	for key, txWrite := range src {
		dst[key] = txWrite
	}

	return dst
}
